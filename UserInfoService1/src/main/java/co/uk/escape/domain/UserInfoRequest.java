package co.uk.escape.domain;

import java.io.Serializable;

public class UserInfoRequest implements Serializable{
	
	private static final long serialVersionUID = 7915992167321675969L;
	private String emailAddress;
	
	public UserInfoRequest(){}
	
	public UserInfoRequest(String emailAddress){
		this.setEmailAddress(emailAddress);
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Override
	public String toString() {
		return "UserInfoRequest [emailAddress=" + emailAddress + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((emailAddress == null) ? 0 : emailAddress.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserInfoRequest other = (UserInfoRequest) obj;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		return true;
	}

}
